# Pic4Review

__Pic4Review__ is a tool for helping you review a geo-dataset for a given task (for example improve OpenStreetMap data) using open-licensed pictures around each feature.

You can try it out at [pic4review.pavie.info](https://pic4review.pavie.info/) (or [develop instance](http://pic4review.pavie.info/dev/)).

[![Help making this possible](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/PanierAvide/donate)

## Purpose

The goal of this tool is to make use of pictures taken by community on various platforms (Mapillary, OpenStreetCam, Wikimedia Commons or Flickr) in order to improve geographic data. As pictures provide a high-level of detail, they are really useful to add qualitative data over existing features. This tool makes these improvements easier for everyone by automating pictures retrieval and association with features to review.


## Contributing

This project is an open source tool, meaning it is made by volunteers. If you want to help us, there are several ways (technical or non-technical ones). You can find how to participate in the [contributing documentation](CONTRIBUTING.md).


## Develop/run your own instance

Pic4Review is already available online for use (see link on top of this document). If you want to run your own, you can grab one of the [recent builds](https://pic4review.pavie.info/builds/). To work on project development, you can read [develop documentation](DEVELOP.md).


## Contributors

Pic4Review exists and is everyday improving thanks to the __amazing people__ following :

* Adrien Pavie, the creator and main developer of Pic4Review
* Jean-Louis Zimmermann, for his ideas and experimenting various use cases
* Florian Lainez, for his ideas on improving the user experience
* Christopher Beddow, for testing the tool and providing Mapillary support
* Nuno Caldeira, for creating documentation ressources
* Binnette, for offering technical help and improving the user experience
* And all users, for using the tool and sharing their ideas !


## License

Copyright 2017-2019 Adrien PAVIE

See [LICENSE](LICENSE.txt) for complete AGPL3 license.

Pic4Review is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pic4Review is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Pic4Review. If not, see http://www.gnu.org/licenses/.
