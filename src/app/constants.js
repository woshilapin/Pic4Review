export default {
	LEAFLET_IMG_PATH: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/',
	TILE_ATTRIBUTION: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors',
	TILE_URL: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
	ID_URL: 'https://www.openstreetmap.org/edit',
	JOSM_URL: 'http://127.0.0.1:8111/load_and_zoom?',
	OAPI_URL: 'https://overpass-api.de/api/interpreter',
	
	// Production config
	OSM_API_URL: 'https://www.openstreetmap.org',
	OAUTH_CONSUMER_KEY: 'KHkPq0Llu63IWjdchiKALkAcDfJUwqi6GHKM9IY6',
	OAUTH_SECRET: 'PWXMH1Ko6vOFFI69wvwv2p9yH8y5Af2cJ8nMkXf0',
	P4R_URL: 'http://localhost:28113',
	
	// Dev config
// 	OSM_API_URL: 'https://master.apis.dev.openstreetmap.org',
// 	OAUTH_CONSUMER_KEY: 'aledk8wE8vD7VDJlpvKdpDvzUDODKtVNN48ic4OT',
// 	OAUTH_SECRET: 'UFAit5VW1VdMF1mRfLVohe9ymfYXUpoJvDCqv1ib',
// 	P4R_URL: 'http://192.168.43.3:28113'
};
